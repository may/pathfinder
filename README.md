# How to use

- `CTRL + click` to create a new point
- `ALT + click` to move the starting point

The program will optimise for a path that visits all of the points, and then returns to the starting point
