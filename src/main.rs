use ::rand::{
    distributions::WeightedIndex, prelude::Distribution, rngs::ThreadRng, thread_rng, Rng,
};
use egui::Visuals;
use fast_poisson::Poisson2D;
use macroquad::prelude::*;

#[derive(Clone, Copy)]
pub struct Config {
    pub pheromone_evaporation: f32,
    pub distance_power: f32,
    pub pheromone_power: f32,
    pub pheromone_intensity: f32,
    pub light_mode: bool,
}

struct Pathfinder {
    distances: Vec<Vec<f32>>,
    pheromone_trails: Vec<Vec<f32>>,
    iteration_number: usize,
    best_path: Vec<(usize, usize)>,
    best_length: f32,
    rng: ThreadRng,
    config: Config,
}

pub struct Navigator {
    points: Vec<(f32, f32)>,
    pathfinder: Pathfinder,
}

fn distance(a: &(f32, f32), b: &(f32, f32)) -> f32 {
    ((a.0 - b.0).powi(2) + (a.1 - b.1).powi(2)).sqrt()
}

impl Pathfinder {
    fn new(points: &[(f32, f32)], config: Config) -> Self {
        let distances = points
            .iter()
            .map(|x| points.iter().map(|y| distance(x, y)).collect::<Vec<_>>())
            .collect::<Vec<_>>();
        let pheromone_trails = vec![vec![1.0; points.len()]; points.len()];
        let iteration_number = 0;
        let best_path = vec![];
        let best_length = f32::INFINITY;
        let rng = thread_rng();

        Self {
            distances,
            pheromone_trails,
            iteration_number,
            best_path,
            best_length,
            rng,
            config,
        }
    }
    fn update(&mut self, points: &[(f32, f32)]) {
        self.iteration_number += 1;
        let mut current_index = 0;
        let mut remaining_indices = (1..points.len()).collect::<Vec<_>>();
        let mut path = vec![];

        while !remaining_indices.is_empty() {
            // Calculates weights for all possible next points
            let mut weights = vec![];
            for next_index in &remaining_indices {
                let distance = self.distances[current_index][*next_index];
                let pheromone_strength = self.pheromone_trails[current_index][*next_index];
                let weight = (1.0 / distance).powf(self.config.distance_power)
                    * pheromone_strength.powf(self.config.pheromone_power);
                weights.push(weight);
            }

            // Chooses next point
            let dist = WeightedIndex::new(&weights).unwrap_or_else(|_| {
                WeightedIndex::new(&vec![1.0; remaining_indices.len()]).unwrap()
            });
            let sampled_index = dist.sample(&mut self.rng);
            let chosen_index = remaining_indices[sampled_index];

            // Adds chosen point to path
            path.push((current_index, chosen_index));
            remaining_indices.remove(sampled_index);
            current_index = chosen_index;
        }
        // Returns path to start
        path.push((current_index, 0));

        // Calculates length of path
        let mut length = 0.0;
        for (start_index, end_index) in &path {
            let start = points[*start_index];
            let end = points[*end_index];
            length += distance(&start, &end);
        }

        // Applies changes in pheromones to paths visited
        let change = self.config.pheromone_intensity / length;
        for (start_index, end_index) in &path {
            self.pheromone_trails[*start_index][*end_index] += change;
        }

        // Applies decay to all trails in map
        for i in 0..points.len() {
            for j in 0..points.len() {
                self.pheromone_trails[i][j] *= 1.0 - self.config.pheromone_evaporation;
            }
        }

        // Updates best path
        if length < self.best_length {
            self.best_length = length;
            self.best_path = path.clone();
        }
    }
}

impl Navigator {
    pub fn new(points: Vec<(f32, f32)>, config: Config) -> Self {
        let pathfinder = Pathfinder::new(&points, config);
        Self { points, pathfinder }
    }
    pub fn update(&mut self) {
        if is_mouse_button_pressed(MouseButton::Left) && is_key_down(KeyCode::LeftControl) {
            self.points.push(mouse_position());
            self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
        } else if is_mouse_button_pressed(MouseButton::Left) && is_key_down(KeyCode::LeftAlt) {
            self.points[0] = mouse_position();
            self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
        }
        if self.points.len() > 1 {
            self.pathfinder.update(&self.points);
        }
        self.render();
    }
    pub fn render(&mut self) {
        clear_background(if self.pathfinder.config.light_mode {
            WHITE
        } else {
            BLACK
        });
        // Render pheromone trails
        let max_trail = self
            .pathfinder
            .pheromone_trails
            .iter()
            .flatten()
            .max_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap_or(&1.0);
        for i in 0..self.points.len() {
            for j in 0..self.points.len() {
                let start = self.points[i];
                let end = self.points[j];
                let strength = self.pathfinder.pheromone_trails[i][j] / max_trail * 3.0;
                draw_line(
                    start.0,
                    start.1,
                    end.0,
                    end.1,
                    strength,
                    Color::from_rgba(255, 0, 0, 255),
                );
            }
        }

        // Draw current best path
        for (start_index, end_index) in self.pathfinder.best_path.iter() {
            let start = self.points[*start_index];
            let end = self.points[*end_index];
            draw_line(
                start.0,
                start.1,
                end.0,
                end.1,
                4.0,
                if self.pathfinder.config.light_mode {
                    BLACK
                } else {
                    WHITE
                },
            );
        }

        // Draw all points
        for point in &self.points {
            draw_circle(
                point.0,
                point.1,
                5.0,
                if self.pathfinder.config.light_mode {
                    BLACK
                } else {
                    WHITE
                },
            );
            draw_circle(
                point.0,
                point.1,
                3.5,
                if self.pathfinder.config.light_mode {
                    WHITE
                } else {
                    BLACK
                },
            );
        }

        egui_macroquad::ui(|ctx| {
            if self.pathfinder.config.light_mode {
                ctx.set_visuals(Visuals::light());
            } else {
                ctx.set_visuals(Visuals::dark());
            }
            // Draw UI to change config
            egui::Window::new("Config")
                .default_open(false)
                .show(ctx, |ui| {
                    ui.add(
                        egui::Slider::new(&mut self.pathfinder.config.distance_power, 0.1..=10.0)
                            .text("Distance power"),
                    );
                    ui.add(
                        egui::Slider::new(&mut self.pathfinder.config.pheromone_power, 0.1..=10.0)
                            .text("Pheromone power"),
                    );
                    ui.add(
                        egui::Slider::new(
                            &mut self.pathfinder.config.pheromone_intensity,
                            1.0..=500.0,
                        )
                        .text("Pheromone intensity"),
                    );
                    ui.add(
                        egui::Slider::new(
                            &mut self.pathfinder.config.pheromone_evaporation,
                            0.1..=0.99,
                        )
                        .text("Pheromone Evaporation"),
                    );
                    ui.checkbox(&mut self.pathfinder.config.light_mode, "Light mode");
                });

            // Draw UI to display current results
            egui::Window::new("Results")
                .default_open(false)
                .show(ctx, |ui| {
                    ui.label(format!("# of points: {}", self.points.len()));
                    ui.label(format!("Best path length: {}", self.pathfinder.best_length));
                    ui.label(format!("Iteration #: {}", self.pathfinder.iteration_number));
                });

            // Draw UI to provide controls to user
            egui::Window::new("Controls")
                .default_open(false)
                .show(ctx, |ui| {
                    if ui.button("Clear all points").clicked() {
                        self.points.clear();
                        self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
                    }
                    if ui.button("Add 10 random points").clicked() {
                        for _ in 0..10 {
                            let x = self.pathfinder.rng.gen_range(0.0..screen_width());
                            let y = self.pathfinder.rng.gen_range(0.0..screen_height());
                            self.points.push((x, y));
                            self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
                        }
                    }
                    if ui.button("Add 100 random points").clicked() {
                        for _ in 0..100 {
                            let x = self.pathfinder.rng.gen_range(0.0..screen_width());
                            let y = self.pathfinder.rng.gen_range(0.0..screen_height());
                            self.points.push((x, y));
                            self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
                        }
                    }
                    if ui
                        .button("Add poisson sampled points, radius 100")
                        .clicked()
                    {
                        let mut points = Poisson2D::new()
                            .with_dimensions([screen_width() as f64, screen_height() as f64], 100.0)
                            .iter()
                            .map(|[x, y]| (x as f32, y as f32))
                            .collect::<Vec<_>>();
                        self.points.append(&mut points);
                        self.pathfinder = Pathfinder::new(&self.points, self.pathfinder.config);
                    }
                });
        });
        egui_macroquad::draw();
    }
}

#[macroquad::main("Navigator")]
async fn main() {
    let config = Config {
        pheromone_evaporation: 0.1,
        distance_power: 4.0,
        pheromone_power: 1.0,
        pheromone_intensity: 100.0,
        light_mode: true,
    };
    let mut navigator = Navigator::new(vec![], config);
    loop {
        navigator.update();
        next_frame().await
    }
}
